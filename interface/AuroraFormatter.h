#ifndef AURORAFORMATTER_H
#define AURORAFORMATTER_H
#include<vector>
using namespace std;
class AuroraFormatter {
    public:
        AuroraFormatter(){};
        vector<bool> apply_blocking(vector<bool> & stream, int chip_id);
        vector<bool> orphan_pad(vector<bool> & stream);

};
#endif /* AURORAFORMATTER_H */
