#ifndef RD53STREAMENCODER_H
#define RD53STREAMENCODER_H
#include<interface/QCore.h>
#include<iostream>
#include<map>
#include<vector>

class RD53StreamEncoder{
    public:
        RD53StreamEncoder();
        void reset();
        void finalize();
        void serialize_event(vector<QCore> & qcores, int event, map<vector<bool>, int>& frequencies, map<vector<bool>, float>& efficiencies, int& nQcore, int& encodedSize, int& hitsPerQcore, int& tempQCore, vector<int>& HitVec, vector<int>& EncVec);
        void set_compression(bool compression);

        vector<bool> get_stream();

    private:
        vector<bool> stream;

        void write_bits(vector<bool> bits_to_write);
        void write_bits(bool bit_to_write);

        vector<bool> serialize_qcore_hitmap(vector<bool> hitmap,  map<vector<bool>, int>& frequencies, map<vector<bool>, float>& efficiencies, int& nQcore, int& encodedSize, int& hitsPerQcore, int& tempQCore, vector<int>& HitVec, vector<int>& EncVec);
        vector<bool> serialize_qcore_tots(vector<uint32_t> adcs);

        void add_orphan_padding();
        bool write_tot = true;
        bool do_compression = true;
};

#endif  // RD53STREAMENCODER_H
