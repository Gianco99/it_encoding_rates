#include <interface/Util.h>
#include <vector>
#include <stdint.h>
#include <assert.h>
#include <math.h>
#include <algorithm>    // std::reverse
using namespace std;


vector<bool> adc_to_binary(uint32_t adc){
    assert(adc < 16);
    vector<bool> bin = {0 ,0 ,0 ,0};

    for(uint32_t i=0; adc>0; i++) {
        bin[i] = adc%2;
        adc = adc/2;
    }
    assert(bin.size()==4);
    return bin;
}

vector<bool> int_to_binary(int an_int, int length){
    vector<bool> bin;
    for(uint32_t i=0; an_int>0; i++) {
        // cout << i <<endl;
        if(i >= length) break;
        bin.push_back(an_int%2);
        an_int = an_int/2;
    }
    while(bin.size() < length){
        bin.push_back(0);
    }
    reverse(bin.begin(),bin.end());
    assert(bin.size()==length);
    return bin;
}

int binary_to_int(vector<bool> bin) {
    uint32_t conv = 0;
    unsigned size = bin.size();
    for(unsigned i = 1; i<=size; i++){
        if(bin.at(size-i)) {
            conv+=pow(2,i-1);
        }
    }
    return conv;
}


IntMatrix empty_matrix(uint32_t nrows, uint32_t ncols){
    IntMatrix matrix;
    matrix.reserve(nrows);
    for (uint32_t r = 0; r < nrows; r++) {
            vector<uint32_t> row;
            row.reserve(ncols);
            for (uint32_t c = 0; c < ncols; c++) {
                  row.push_back(0);
            }
            assert(row.size()==ncols);
            matrix.push_back(row);
    }
    assert(matrix.size()==nrows);
    return matrix;
}


