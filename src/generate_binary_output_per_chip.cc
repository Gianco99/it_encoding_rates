#include <iostream>
#include <numeric>
#include <exception>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include<TFile.h>
#include<TTreeReader.h>
#include<TTreeReaderArray.h>
#include<include/cxxopts.hpp>
#include<interface/Util.h>
#include<interface/ModuleTTreeUnpacker.h>
#include<interface/QCoreFactory.h>
#include<interface/RD53StreamEncoder.h>
#include<interface/AuroraFormatter.h>
#include<map>
#include<vector>

using namespace std;

uint64_t append_bool_to_uint64(uint64_t acc, bool new_bool) {
    return ( acc << 1 ) | new_bool;
}

// debugging purpose
void print_stream(vector<bool> stream) {
    cout<<"New stream:"<<endl;
    for (int ibit=0; ibit<stream.size(); ibit++) {
        cout<<stream[ibit];
        if ((ibit+1)%64==0) cout<<endl;
    }
    cout<<endl;
}

int main(int argc, char *argv[]){

    string filepath("itdigi/itdigi_TTBar14TeV_10_6_0_patch2_D41_PU200.root");
    string treepath("BRIL_IT_Analysis/Digis");
    string output_dirname("");
    string config_filename("config/default.config");
    string tag("");
    int DTC = 11;
    int nevents = -1;

    // argument parsing
    std::string help_msg("Usage: ./bin/generate_binary_output_per_chip [options]\n\
        --help:     display this message.\n\
        --input/-i INPUT_FILENAME:  Change the input file name, by default uses itdigi/itdigi_TTBar14TeV_10_6_0_patch2_D41_PU200.root.\n\
        --nevents/-n NEVENTS:       set the number of events to run on, by default run all available events in file.\n\
        --dtc/-d DTC_ID:            specify the dtc to generate binary streams, should be a 2-digit number, default value is 11.\n\
        --tag/-t TAG:            specify a tag string to be appened to output file name.\n\
        --output/-o OUT_DIR:            specify the exact output directory name.\n\
        --config/-c CONFIG_FILE:    Config file that include n-elinks and n-events-compression per chip, by default uses config/default.config.");
    for (int iarg =0; iarg<argc; iarg++) {
        if (iarg==0) continue;
        if (std::string(argv[iarg])=="--help") {std::cerr<<help_msg<<std::endl; return 0;}
        if (std::string(argv[iarg])=="--input" || std::string(argv[iarg])=="-i") {
            if (iarg+1 < argc) {
                filepath = argv[++iarg];
            }
            else {
                std::cerr<<"--input/-i option requires one argument."<<std::endl;
                return 1;
            }
            continue;
        }
        if (std::string(argv[iarg])=="--nevents" || std::string(argv[iarg])=="-n") {
            if (iarg+1 < argc) {
                std::string input_nevents_str(argv[++iarg]);
                nevents = stoi(input_nevents_str);
            }
            else {
                std::cerr<<"--nevents/-n option requires one argument."<<std::endl;
                return 1;
            }
            continue;
        }
        if (std::string(argv[iarg])=="--dtc" || std::string(argv[iarg])=="-d") {
            if (iarg+1 < argc) {
                std::string input_dtc_str(argv[++iarg]);
                DTC = stoi(input_dtc_str);
            }
            else {
                std::cerr<<"--dtc/-d option requires one argument."<<std::endl;
                return 1;
            }
            continue;
        }
        if (std::string(argv[iarg])=="--config" || std::string(argv[iarg])=="-c") {
            if (iarg+1 < argc) {
                config_filename = argv[++iarg];
            }
            else {
                std::cerr<<"--config/-c option requires one argument."<<std::endl;
                return 1;
            }
            continue;
        }
        if (std::string(argv[iarg])=="--tag" || std::string(argv[iarg])=="-t") {
            if (iarg+1 < argc) {
                tag = string("_") + argv[++iarg];
            }
            else {
                std::cerr<<"--tag/-t option requires one argument."<<std::endl;
                return 1;
            }
            continue;
        }
        if (std::string(argv[iarg])=="--output" || std::string(argv[iarg])=="-o") {
            if (iarg+1 < argc) {
                output_dirname = argv[++iarg];
            }
            else {
                std::cerr<<"--output/-o option requires one argument."<<std::endl;
                return 1;
            }
            continue;
        }
        std::cerr<<"Unknow option/argument: "<<argv[iarg]<<std::endl;
        return 2;
    }



    std::cout<<"Reading input file:"<<filepath<<" ..."<<std::endl;
    ModuleTTreeUnpacker unpacker(filepath, treepath);

    if (nevents<=0 || nevents>unpacker.get_Nevents()) {
        nevents = unpacker.get_Nevents();
        std::cout<<"Running on all events on file: "<<nevents<<std::endl;
    }
    if (output_dirname==""){
        output_dirname = ("chip_streams");
        output_dirname += "_DTC";
        output_dirname += to_string(DTC);
        output_dirname += "_";
        string nevents_string = to_string(nevents);
        if (nevents % 1000 == 0) nevents_string = to_string(nevents/1000)+"k";
        output_dirname += nevents_string;
        output_dirname += "evt";
        output_dirname += tag;
    }
    std::cout<<"Using output dir: "<<output_dirname<<std::endl;
    if(mkdir(output_dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)==-1) {
        if (errno!=EEXIST) std::cerr<<"Cannot create folder:"<<output_dirname<<std::endl;
    }


    QCoreFactory qfactory;
    RD53StreamEncoder encoder;
    AuroraFormatter aurora;
    // tuple<dtc, isBarrel, layer, disk, module, chip>
    std::map<std::tuple<int,bool,int,int,int,int>,std::ofstream*> output_map; //(outputpath, std::ios::binary);


    int ievent = 0;
    while(unpacker.next_event()) {
        int event = unpacker.get_event();
        ievent++;
        if(ievent>nevents) break;
        int barWidth = 70;
        std::cout << "[";
        int pos = barWidth * ievent/nevents;
        for (int i = 0; i < barWidth; ++i) {
            if (i < pos) std::cout << "=";
            else if (i == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "] " << ievent <<"/"<< nevents << " %\r";
        std::cout.flush();

        for(int imod=0; imod<unpacker.get_nmodule(); imod++){
            auto pm = unpacker.get_module(imod);
            // process dtc11 related modules for now
            if(pm.dtc != DTC) continue;
            auto qcores = qfactory.from_pixel_module_perchip(pm);
            int nchips = pm.get_nchips();
            assert(nchips == qcores.size());
            for (int ichip=0; ichip<nchips; ichip++){
                // Output binary files organized by dtc and elink, select the correct file to write
                std::ofstream* output;
                auto key = std::make_tuple(pm.dtc, pm.barrel, pm.layer, pm.disk, pm.module_id, ichip);
                if (output_map.find(key) == output_map.end() ){
                    // create a new file if not opened already
                    string output_filename = output_dirname + string("/dtc") + to_string(pm.dtc) + string("isBarrel") + to_string(pm.barrel) 
                        + string("layer") + to_string(pm.layer) + string("disk") + to_string(pm.disk) + string("module") + to_string(pm.module_id)
                        + string("chip") + to_string(ichip) + string(".bin");
                    output = new std::ofstream(output_filename, std::ios::binary);
                    output_map[key] = output;
                }
                else {
                    output = output_map[key];
                }

                // Build the stream
                encoder.reset();
                //encoder.set_compression(false);
		std::map<std::vector<bool>, int> frequencies;
		std::map<std::vector<bool>, float> efficiencies;
		int nQcore;
		int hitsPerQcore;
		int encodedSize;
		std::vector<int> HitVec;
		std::vector<int> EncVec;
		int tempQCore;
                encoder.serialize_event(qcores[ichip], event, frequencies, efficiencies, nQcore, hitsPerQcore, encodedSize, tempQCore, HitVec, EncVec);
                auto stream_chip_raw = encoder.get_stream();
                auto stream_chip_aurora_unpad = aurora.apply_blocking(stream_chip_raw, ichip);
                auto stream = aurora.orphan_pad(stream_chip_aurora_unpad);

                auto flag_begin = stream.begin();
                while (flag_begin+63 <= stream.end()) {
                    uint64_t value = std::accumulate( flag_begin, flag_begin+64, 0ull, append_bool_to_uint64);
                    output->write( reinterpret_cast<const char*>(&value), sizeof(value) ) ;
                    flag_begin += 64;
                }
            }

        }

    }
    // close output files
    for (auto it=output_map.begin(); it!=output_map.end(); it++) it->second->close();
}
