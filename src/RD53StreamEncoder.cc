#include<interface/RD53StreamEncoder.h>
#include<assert.h>
#include<interface/LUT.h>
#include<iostream>
#include<map>
#include<vector>

RD53StreamEncoder::RD53StreamEncoder(){
    this->stream.reserve(128*64);
};
vector<bool> RD53StreamEncoder::get_stream() {
    return stream;
}
void RD53StreamEncoder::reset(){
    stream.clear();
}
void RD53StreamEncoder::set_compression(bool compression) {
    this->do_compression = compression;
}
void RD53StreamEncoder::write_bits(vector<bool> bits_to_write) {
    stream.insert(
                  stream.end(),
                  bits_to_write.begin(),
                  bits_to_write.end());
};
void RD53StreamEncoder::write_bits(bool bit_to_write) {
    stream.push_back(bit_to_write);
};


vector<bool> RD53StreamEncoder::serialize_qcore_hitmap(vector<bool> hitmap, std::map<std::vector<bool>, int>& frequencies, std::map<std::vector<bool>, float>& efficiencies, int& nQcore, int& encodedSize, int& hitsPerQcore, int& tempQCore, std::vector<int>& HitVec, std::vector<int>& EncVec) {
    assert(hitmap.size()==16);
    // Separate rows
    vector<bool> row1, row2;
    row1.insert(row1.end(), hitmap.begin(), hitmap.begin()+8);
    row2.insert(row2.end(), hitmap.begin()+8, hitmap.end());
    assert(row1.size()==8 and row2.size()==8);

    // Row OR information
    std::vector<bool> row_or = {0, 0};
    for( auto const bit : row1) {
        if(bit) {
            row_or[0] = 1;
            break;
        }
    }
    for( auto const bit : row2) {
        if(bit) {
            row_or[1] = 1;
            break;
        }
    }

    // Compress the components
    vector<bool> row_or_enc = enc2(row_or);
    vector<bool> row1_enc = enc8(row1);
    vector<bool> row2_enc = enc8(row2);

    vector<bool> encoded_hitmap;
    encoded_hitmap.reserve(row_or_enc.size() + row1_enc.size() + row2_enc.size());
    encoded_hitmap.insert(encoded_hitmap.end(), row_or_enc.begin(), row_or_enc.end());
    encoded_hitmap.insert(encoded_hitmap.end(), row1_enc.begin(), row1_enc.end());
    encoded_hitmap.insert(encoded_hitmap.end(), row2_enc.begin(), row2_enc.end());

    int hitmap_sum=0;
    for(int i=0;i<16;i++){
      if(hitmap[i]) hitmap_sum++;
    }
    hitsPerQcore += hitmap_sum;
    HitVec.push_back(hitmap_sum);
    encodedSize += encoded_hitmap.size();
    EncVec.push_back(encoded_hitmap.size());
    nQcore += 1;
    tempQCore += 1;
    if (!frequencies.count(hitmap)) {
      frequencies[hitmap] = 1;
    }
    else{
      frequencies[hitmap]++;
    }
    efficiencies[hitmap] = 1.0*(encoded_hitmap.size() + hitmap_sum)/(16 + hitmap_sum);
    return encoded_hitmap;
};

void RD53StreamEncoder::add_orphan_padding() {
    /* 
    Zero-padding of current stream to multiple of 64 bits.
    */
    int bits_written = stream.size();
    int n_orphan = 63 - bits_written % 63;
    vector<bool> orphan_bits(n_orphan, false);
    write_bits(orphan_bits);
}
vector<bool> RD53StreamEncoder::serialize_qcore_tots(vector<uint32_t> adcs) {
    vector<bool> serialized_tots;
    for(auto const & adc: adcs){
        if(adc==0){
            continue
        }
        vector<bool> adc_binary = adc_to_binary(adc);
        serialized_tots.insert(
                                serialized_tots.end(),
                                adc_binary.begin(),
                                adc_binary.end()
                                );
    }
    return serialized_tots;
}


void RD53StreamEncoder::serialize_event(vector<QCore> & qcores, int event, std::map<std::vector<bool>, int> &frequencies, std::map<std::vector<bool>, float> &efficiencies, int& nQcore, int& encodedSize, int& hitsPerQcore, int& tempQCore, std::vector<int>& HitVec, std::vector<int>& EncVec){

    bool is_new_ccol = true;
    int bits_written = 0;
    vector<bool> event_tag = int_to_binary(event, 8);
    write_bits(event_tag);

    int nqcore=0;
    for(auto q: qcores){

        // ccol address
        if(is_new_ccol){
            vector<bool> ccol_address = int_to_binary(53, 6);
            write_bits(ccol_address);
        }
        // islast / isneighbour
        write_bits(q.islast);
        write_bits(q.isneighbour);

        // qrow address only if not neighbour
        if(not q.isneighbour){
            vector<bool> qrow_address = int_to_binary(q.qcrow, 8);
            write_bits(qrow_address);
        }

        // Binary-tree encoded hitmap
        if (this->do_compression) {
	  auto serialized_hitmap = serialize_qcore_hitmap(q.get_hitmap(), frequencies, efficiencies, nQcore, encodedSize, hitsPerQcore, tempQCore, HitVec, EncVec);
            write_bits(serialized_hitmap);
        }
        else {
            write_bits(q.get_hitmap());
        }

        // ToT values if wanted
        if(write_tot){
            auto serialized_tots = serialize_qcore_tots(q.adcs);
            write_bits(serialized_tots);
        }

        // Next qcore needs to print ccol address
        // if this qcore is the last in its column
        is_new_ccol = q.islast;
    }

}
void RD53StreamEncoder::finalize(){
    this->add_orphan_padding();
}
