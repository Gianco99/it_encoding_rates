#include <interface/QCoreFactory.h>
#include <interface/PixelModule.h>
#include <iostream>
#include <tuple>
using namespace std;


QCoreFactory::QCoreFactory(int col_factor, int row_factor){
    this->col_factor = col_factor;
    this->row_factor = row_factor;
};

std::pair<int, int> remap_sensor_coordinates(int row_factor_in, int col_factor_in, int row_factor_out, int col_factor_out, int row, int col) {
    int new_row, new_col;
    if(row_factor_in==4 and col_factor_in==4) {
        // map 4X4 sensor coordinates to 2X8 readout coordinates
        new_row = row/2;
        new_col = col*2 + row%2;
    } else {
        // Not implemented
        throw;
    }
    return std::make_pair(new_row, new_col);
}

// This function loop throgh the sparse matrix of the module pixels
// Pixel mapping INFO:
//      In CMSSW the pixels are mapped to sensors
//      Dimension for 2 chips module =  672X434 = 672X(217+217)
//      Dimension for 4 chips module = 1354X434 = (677+677)X(217+217)
//      Due to unimplemented large pixels, each chip has 1 extra column and 5 extra rows if neighboring another chip
//      For now we ignore these extra pixels
//      Quarter core definition is 2X8 for readout mapping and 4X4 for sensor(CMSSW) mapping
//      So we will map 4X4 back to 2X8 then construct quarter core from there
// Function Arguments:
//      nrows_perchip: number of rows per chip, negative value means there is only one row of chips in module (i.e. nrows_perchip == pm.nrows)
//      ncols_perchip: number of cols per chip, negative value means there is only one col of chips in module (i.e. ncols_perchip == pm.ncols)
//      top_edges: the lowest row numbers for each chip. can be used to skip certain rows
//      left_edges: the lowest col numbers for each chip. can be used to skip certain cols
vector<vector<QCore>> QCoreFactory::loop_module_pixels(PixelModule & pm, int nrows_perchip, int ncols_perchip, vector<int> top_edges, vector<int> left_edges) {
    // Determine the layout of chip splitting, whether it's 2X1 or 2X2 or other schemes
    if (nrows_perchip<0) nrows_perchip = pm.nrows;
    if (ncols_perchip<0) ncols_perchip = pm.ncols;
    int nchip_rows = floor(pm.nrows / nrows_perchip);
    int nchip_cols = floor(pm.ncols / ncols_perchip);
    // Make sure we get the right number of chips per module
    int nchips = nchip_rows * nchip_cols;
    if (nchips>1) assert(nchips == pm.get_nchips());
    
    // Calculate the dimension of number of quarter-cores per chip
    int nqcrows_perchip = floor(nrows_perchip / this->row_factor);
    int nqccols_perchip = floor(ncols_perchip / this->col_factor);
    int max_nqc_perchip = nqcrows_perchip * nqccols_perchip;

    // Number of hit entries should be identical in the three vectors;
    int nentries = pm.row.size();
    assert(nentries == pm.column.size());
    assert(nentries == pm.adc.size());

    // Setup some intermediate buffer for qcore adcs and track if the whole column is empty or not
    // the row number always increase first, then the column number
    // We construct a column of qcores everytime we have all nrows*this->col_factor of pixels
    // And the buffers are organized as the nth chip in the column
    //
    // On each column of qcore, we have      this number of chips in rows    this number of qcores for each chip    this number of pixels per qcore
    vector<vector<vector<uint32_t>>> adcs_perqc_buffer(nchip_rows, vector<vector<uint32_t>>(nqcrows_perchip, vector<uint32_t>(this->col_factor*this->row_factor, 0ul)));
    vector<vector<bool>>        nonzero_qc_buffer(nchip_rows, vector<bool>       (nqcrows_perchip, false));

    // The vector of constructed qcores per chip as function return
    vector<vector<QCore>> qcores(nchips, vector<QCore>());
    for (int ichip=0;ichip<nchips;ichip++) qcores[ichip].reserve(128); // rough estimation of size, for time performance

    // Loop through hit entries and construct qcores, the hit entries are ordered first in column, then in rows
    for (uint64_t ientry =0; ientry<nentries; ientry++) {
        // row, col, adc from the sparse matrix
        int row_in_module = pm.row[ientry];
        int col_in_module = pm.column[ientry];
        uint32_t adc = pm.adc[ientry];
	if (adc == 0) {adc = 1; }
        // determine which chip is the current pixel located
        int ichip_row = 0, ichip_col=0;
        while ((row_in_module>=top_edges[ichip_row]+nrows_perchip && ichip_row+1<nchip_rows)) ichip_row++;
        while ((col_in_module>=left_edges[ichip_col]+ncols_perchip && ichip_col+1<nchip_cols)) ichip_col++;
        if (row_in_module<top_edges[ichip_row] || row_in_module>=top_edges[ichip_row]+nrows_perchip) continue; // in case this is an ignored pixel
        if (col_in_module<left_edges[ichip_col] || col_in_module>=left_edges[ichip_col]+ncols_perchip) continue; // in case this is an ignored pixel
        // coordinates within the chip
        int row_in_chip = row_in_module - top_edges[ichip_row];
        int col_in_chip = col_in_module - left_edges[ichip_col];
        int qcrow_in_chip = row_in_chip/this->row_factor;
        int qccol_in_chip = col_in_chip/this->col_factor;
        int row_in_qc = row_in_chip%this->row_factor;
        int col_in_qc = col_in_chip%this->col_factor;
        // map 4X4 sensor coordinates to 2X8 readout coordinates
        assert(this->row_factor==4 && this->col_factor==4);
        std::tie(row_in_qc, col_in_qc) = remap_sensor_coordinates(4,4,2,8, row_in_qc, col_in_qc);
        // store the adc value in buffer
        nonzero_qc_buffer[ichip_row][qcrow_in_chip] = true;
        adcs_perqc_buffer[ichip_row][qcrow_in_chip][row_in_qc*8 + col_in_qc] = adc;
        // push back the current column of qcores if we are at the end of this column
        if (ientry+1==nentries || (pm.column[ientry+1]-left_edges[ichip_col])/this->col_factor > qccol_in_chip) {
            for (ichip_row=0; ichip_row<nchip_rows; ichip_row++) {
                int ichip = ichip_row + nchip_rows*ichip_col;
                int previous_qcrow = -2; //to track the neighboring between qcores
                for (qcrow_in_chip=0; qcrow_in_chip<nqcrows_perchip; qcrow_in_chip++) {
                    if (!nonzero_qc_buffer[ichip_row][qcrow_in_chip]) continue;
                    // Construct qcores with arguments: qccol, qcrow, is_neighbor, is_last(will set this latter), vector of adcs
                    qcores[ichip].push_back(QCore(qccol_in_chip, qcrow_in_chip, (previous_qcrow+1==qcrow_in_chip), false, adcs_perqc_buffer[ichip_row][qcrow_in_chip]));
                    previous_qcrow = qcrow_in_chip;
                    // empty out the buffers
                    nonzero_qc_buffer[ichip_row][qcrow_in_chip] = false;
                    std::fill(adcs_perqc_buffer[ichip_row][qcrow_in_chip].begin(), adcs_perqc_buffer[ichip_row][qcrow_in_chip].end(), 0);
                }
                // Now set the "is_last" property of the last qcore
                if (qcores[ichip].size()>0) qcores[ichip][qcores[ichip].size()-1].islast=true;
            }
        }
    }
    return qcores;
}


vector<QCore> QCoreFactory::from_pixel_module(PixelModule & pm) {
    vector<vector<QCore>> qcores = QCoreFactory::loop_module_pixels(pm, -1, -1, {0}, {0});
    assert(qcores.size()==1);
    return qcores[0];
}


// Same function as above but output organized per chip (2 to 4 chips depending on geographic location of module)
vector<vector<QCore>> QCoreFactory::from_pixel_module_perchip(PixelModule & pm) {
    int nchips = pm.get_nchips();
    int nrows_perchip = 336 * 2;
    int ncols_perchip = 432 / 2;
    vector<int> left_edges = {0, ncols_perchip+2}; // ignore 2 extra columns at the chip boundary
    vector<int> top_edges = {0};
    if (nchips==4) top_edges.push_back(nrows_perchip+10); //ignore 10 extra rows at the chip boundary
    vector<vector<QCore>> qcores = QCoreFactory::loop_module_pixels(pm, nrows_perchip, ncols_perchip, top_edges, left_edges);
    assert(qcores.size() == nchips);
    return qcores;
}
